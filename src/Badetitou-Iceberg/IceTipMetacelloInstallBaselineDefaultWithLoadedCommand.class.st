Class {
	#name : #IceTipMetacelloInstallBaselineDefaultWithLoadedCommand,
	#superclass : #IceTipMetacelloInstallBaselineCommand,
	#category : #'Badetitou-Iceberg'
}

{ #category : #private }
IceTipMetacelloInstallBaselineDefaultWithLoadedCommand >> basicInstallBaseline: icePackage groups: anArray [

	| packageDir sourceDirectory |
	sourceDirectory := icePackage repository project sourceDirectory.
	sourceDirectory ifEmpty: [ sourceDirectory := '.'].

	packageDir := (icePackage repository location / sourceDirectory) fullName.

	Metacello new
		repository: 'gitlocal://', packageDir;
		baseline: icePackage metacelloBaselineName;
		onConflict: [ :ex | ex useLoaded ];
		onUpgrade: [ :ex | ex useLoaded ];
		onDowngrade: [ :ex | ex useLoaded ];
		load: anArray
]

{ #category : #private }
IceTipMetacelloInstallBaselineDefaultWithLoadedCommand >> execute [

	self installBaseline: self package
]
