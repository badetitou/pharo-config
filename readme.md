# PharoConfig

## Installation 

```smalltalk
Metacello new
    repository: 'gitlab://badetitou/pharo-config:main/src';
    baseline: 'BadetitouPharoConfig';
    load
```

## Packages

- [Pharo-DraculaTheme](https://github.com/badetitou/Pharo-DraculaTheme)
- [Native Notification](https://github.com/badetitou/PharoNativeNotification)
