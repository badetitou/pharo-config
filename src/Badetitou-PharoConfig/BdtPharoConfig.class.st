Class {
	#name : #BdtPharoConfig,
	#superclass : #Object,
	#category : #'Badetitou-PharoConfig'
}

{ #category : #'class initialization' }
BdtPharoConfig class >> initialize [
	self initializeIcebergCredentialsStoreSettings
]

{ #category : #'class initialization' }
BdtPharoConfig class >> initializeIcebergCredentialsStoreSettings [

	<script>
	IceCredentialStore storeFile:
		self sharedRepositoryFolder / #iceberg / ('credentials-', Smalltalk version, '.fuel').
	IceCredentialStore current storeFile:
		self sharedRepositoryFolder / #iceberg / ('credentials-', Smalltalk version, '.fuel')
]

{ #category : #'class initialization' }
BdtPharoConfig class >> resourcesFolder [

	^ self rootFolder / 'SettingRessources'
]

{ #category : #'class initialization' }
BdtPharoConfig class >> rootFolder [

	^ StartupPreferencesLoader preferencesGeneralFolder
]

{ #category : #'class initialization' }
BdtPharoConfig class >> sharedRepositoryFolder [

	| file |
	file := self resourcesFolder / 'localisation'.
	(file exists not or: [
		 ((STON fromString: file contents) at: #dir) isNil ]) ifTrue: [
		^ (FileLocator documents asFileReference / 'git')
			  ensureCreateDirectory ].
	^ ((STON fromString: file contents) at: #dir) asFileReference
]
