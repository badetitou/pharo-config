Class {
	#name : #IceTipMetacelloInstallBaselineDefaultWithIncomingCommand,
	#superclass : #IceTipMetacelloInstallBaselineCommand,
	#category : #'Badetitou-Iceberg'
}

{ #category : #executing }
IceTipMetacelloInstallBaselineDefaultWithIncomingCommand >> basicInstallBaseline: icePackage groups: anArray [

	| packageDir sourceDirectory |
	sourceDirectory := icePackage repository project sourceDirectory.
	sourceDirectory ifEmpty: [ sourceDirectory := '.'].

	packageDir := (icePackage repository location / sourceDirectory) fullName. 
	Metacello new
		repository: 'gitlocal://', packageDir;
		baseline: icePackage metacelloBaselineName;
		onConflict: [ :ex | ex useIncoming ];
		onUpgrade: [ :ex | ex useIncoming ];
		onDowngrade: [ :ex | ex useLoaded ];
		load: anArray
]

{ #category : #executing }
IceTipMetacelloInstallBaselineDefaultWithIncomingCommand >> execute [

	self installBaseline: self package
]
