Class {
	#name : #BaselineOfBadetitouPharoConfig,
	#superclass : #BaselineOf,
	#category : #BaselineOfBadetitouPharoConfig
}

{ #category : #baselines }
BaselineOfBadetitouPharoConfig >> baseline: spec [

	<baseline>
	spec for: #common do: [
		self defineDependencies: spec.
		self definePackages: spec.
		self defineGroups: spec ]
]

{ #category : #baselines }
BaselineOfBadetitouPharoConfig >> defineDependencies: spec [

	spec
		baseline: 'DraculaTheme'
		with: [
		spec repository: 'github://badetitou/Pharo-DraculaTheme:main/src' ].
	spec baseline: 'NativeNotification' with: [
		spec repository:
			'github://badetitou/PharoNativeNotification:main/src' ].
	spec baseline: 'NativeFileChooser' with: [
		spec repository:
			'github://badetitou/PharoNative-FileChooser:main/src' ].
	spec baseline: 'NativeFileChooser' with: [
		spec repository:
			'github://badetitou/PharoNative-FileChooser:main/src' ].
	spec
		baseline: 'MoreLogger'
		with: [ spec repository: 'github://badetitou/MoreLogger:main/src' ]
]

{ #category : #baselines }
BaselineOfBadetitouPharoConfig >> defineGroups: spec [
	
]

{ #category : #baselines }
BaselineOfBadetitouPharoConfig >> definePackages: spec [

	spec package: 'Badetitou-PharoConfig' with: [
		spec requires:
			#( 'DraculaTheme' 'NativeNotification' 'NativeFileChooser'
			   'MoreLogger' ) ].
	spec package: 'Badetitou-Iceberg'
]
