Class {
	#name : #IceTipMetacelloInstallBaselineGroupWithLoadedCommand,
	#superclass : #IceTipMetacelloInstallBaselineCommand,
	#category : #'Badetitou-Iceberg'
}

{ #category : #private }
IceTipMetacelloInstallBaselineGroupWithLoadedCommand >> basicInstallBaseline: icePackage groups: anArray [

	| packageDir sourceDirectory |
	sourceDirectory := icePackage repository project sourceDirectory.
	sourceDirectory ifEmpty: [ sourceDirectory := '.'].

	packageDir := (icePackage repository location / sourceDirectory) fullName.

	Metacello new
		repository: 'gitlocal://', packageDir;
		baseline: icePackage metacelloBaselineName;
		onConflict: [ :ex | ex useLoaded ];
		onUpgrade: [ :ex | ex useLoaded ];
		onDowngrade: [ :ex | ex useLoaded ];
		load: anArray
]

{ #category : #private }
IceTipMetacelloInstallBaselineGroupWithLoadedCommand >> execute [

	| result |
	result := UIManager default 
		request: 'Groups to install (a comma separated string)' 
		initialAnswer: '' 
		title: 'Enter groups to install'.
	result ifNil: [ ^ self ].
	
	self 
		installBaseline: self package
		groups: ((result substrings: ',') 
			collect: #trimmed 
			as: Array)
]
