Class {
	#name : #IceTipMetacelloInstallBaselineGroupWithIncomingCommand,
	#superclass : #IceTipMetacelloInstallBaselineCommand,
	#category : #'Badetitou-Iceberg'
}

{ #category : #private }
IceTipMetacelloInstallBaselineGroupWithIncomingCommand >> basicInstallBaseline: icePackage groups: anArray [

	| packageDir sourceDirectory |
	sourceDirectory := icePackage repository project sourceDirectory.
	sourceDirectory ifEmpty: [ sourceDirectory := '.'].

	packageDir := (icePackage repository location / sourceDirectory) fullName.

	Metacello new
		repository: 'gitlocal://', packageDir;
		baseline: icePackage metacelloBaselineName;
		onConflict: [ :ex | ex useIncoming ];
		onUpgrade: [ :ex | ex useIncoming ];
		onDowngrade: [ :ex | ex useLoaded ];
		load: anArray
]

{ #category : #private }
IceTipMetacelloInstallBaselineGroupWithIncomingCommand >> execute [

	| result |
	result := UIManager default 
		request: 'Groups to install (a comma separated string)' 
		initialAnswer: '' 
		title: 'Enter groups to install'.
	result ifNil: [ ^ self ].
	
	self 
		installBaseline: self package
		groups: ((result substrings: ',') 
			collect: #trimmed 
			as: Array)
]
