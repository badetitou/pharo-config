Extension { #name : #IceTipMetacelloInstallCommandBuilder }

{ #category : #'*Badetitou-Iceberg' }
IceTipMetacelloInstallCommandBuilder >> addCommandsForPackage: aPackage intoGroup: aSpCommandGroup [ 
	
	| defaultBaselineCommand groupBaselineCommand baselineName defaultWithIncomingBaselineCommand groupWithLoadedBaselineCommand groupWithIncomingBaselineCommand defaultWithLoadedBaselineCommand |
	
	baselineName := aPackage name allButFirst: 'BaselineOf' size.
	
	defaultBaselineCommand := IceTipMetacelloInstallBaselineDefaultCommand new.
	defaultBaselineCommand name: ('Install baseline of {1} (Default)' format: { baselineName }).
	defaultBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: defaultBaselineCommand asSpecCommand.
	
	defaultWithIncomingBaselineCommand := IceTipMetacelloInstallBaselineDefaultWithIncomingCommand new.
	defaultWithIncomingBaselineCommand name: ('Install baseline of {1} with incoming (Default)' format: { baselineName }).
	defaultWithIncomingBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: defaultWithIncomingBaselineCommand asSpecCommand.

	defaultWithLoadedBaselineCommand := IceTipMetacelloInstallBaselineDefaultWithLoadedCommand new.
	defaultWithLoadedBaselineCommand name: ('Install baseline of {1} with loaded (Default)' format: { baselineName }).
	defaultWithLoadedBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: defaultWithLoadedBaselineCommand asSpecCommand.
	
	groupBaselineCommand := IceTipMetacelloInstallBaselineGroupCommand new.
	groupBaselineCommand name: ('Install baseline of {1} ...' format: { baselineName }).
	groupBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: groupBaselineCommand asSpecCommand.
	
	groupWithIncomingBaselineCommand := IceTipMetacelloInstallBaselineGroupWithIncomingCommand new.
	groupWithIncomingBaselineCommand name: ('Install baseline of {1} with incoming ...' format: { baselineName }).
	groupWithIncomingBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: groupWithIncomingBaselineCommand asSpecCommand.
	
	groupWithLoadedBaselineCommand := IceTipMetacelloInstallBaselineGroupWithLoadedCommand new.
	groupWithLoadedBaselineCommand name: ('Install baseline of {1} with loaded ...' format: { baselineName }).
	groupWithLoadedBaselineCommand packageModel: aPackage.
	aSpCommandGroup register: groupWithLoadedBaselineCommand asSpecCommand.
]
